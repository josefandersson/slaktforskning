const router = require('express').Router();
const fs = require('fs');
const path = require('path');
const uuid = require('uuid');

const config = require('../config');

const sessions = {};
const ips = {}; // Limit unsuccessful codes to X per hour per ip

const isValidSid = sid => sid != null && sessions[sid];
const isLan = ip => /127.0.0.1|::1|(::ffff:)?192.168.1.[0-9]{1,3}/.test(ip);
const isIpLimited = ip => ips[ip]
	? (ips[ip] = ips[ip].filter(time => Date.now() - 3_600_000 < time)).length >= config.IP_AUTH_ATTEMPTS_PER_HOUR
	: false;

router.get('/login', (req, res) =>
	isValidSid(req.cookies.sid)
		? res.redirect('/')
		: res.render('login'));

router.get('/logout', (req, res) => {
	isValidSid(req.cookies.sid) && delete sessions[req.cookies.sid]
	res.redirect('/');
});

router.post('/login', (req, res) => {
	if (!req.body.accessCode) return res.render('login', { error: 'missingAccessCode', prevPath: req.body.prevPath });

	const ip = req.ipInfo.ip;
	if (!isLan(ip) && isIpLimited(ip)) return res.render('login', { error: 'ipLimit', prevPath: req.body.prevPath });

	const codes = fs.readFileSync(path.resolve(__dirname, '..', config.PATH_ACCESS_CODES)).toString().split('\n').filter(x => x.length && x[0] !== ' ').map(x => x.replace(/ .*$/, ''));
	if (codes.indexOf(req.body.accessCode.trim()) > -1) {
		if (isValidSid(req.cookies.sid))
			delete sessions[req.cookies.sid];

		const sid = uuid.v4();
		sessions[sid] = Date.now();
		res.cookie('sid', sid, { maxAge: config.SESSION_LIFESPAN });

		req.body.prevPath
			? res.redirect(req.body.prevPath)
			: res.redirect('/');
	} else {
		!isLan(ip) && ips[ip]
			? ips[ip].push(Date.now())
			: ips[ip] = [Date.now()];
		res.render('login', { error: 'invalidAccessCode', prevPath: req.body.prevPath });
	}
});

router.use((req, res, next) => {
	const sid = req.cookies.sid;

	req.isLan = isLan(req.ipInfo.ip);
	req.isEn = req.isLan
		? false
		: req.ipInfo.country == null || req.ipInfo.country !== 'SE'

	if (isValidSid(sid)) {
		req.isValidSid = true;
		sessions[sid] = Date.now();
		res.cookie('sid', sid, { maxAge: config.SESSION_LIFESPAN });
		next();
	} else {
		res.render('login', { prevPath: req.path });
	}
});

// Clean old sessions and login attempts once everyday
setInterval(() => {
	const sessionLimit = Date.now() - config.SESSION_LIFESPAN;
	Object.entries(sessions).forEach((sid, lastTime) => {
		if (lastTime < sessionLimit)
			delete sessions[sid];
	});
	const authLimit = Date.now() - 3_600_000;
	Object.entries(ips).forEach((ip, attempts) => {
		const validAttempts = attempts.filter(time => authLimit < time);
		validAttempts.length
			? ips[ip] = validAttempts
			: delete ips[ip];
	});
}, 1000 * 60 * 60 * 24);

module.exports = router;
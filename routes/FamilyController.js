const router = require('express').Router();

const path = require('path');
const config = require('../config');

const Families = require('../src/Families');

router.get('/', (_, res) => {
	// Show families list
	res.send(Families.pages.indexPage);
});

router.get('/:familyname', (req, res) => {
	// Show files for family
	Families.pages[req.params.familyname]
		? res.send(Families.pages[req.params.familyname])
		: res.status(404).send('Unknown page');
});

router.get('/fil/*', (req, res) => {
	const urlname = req.params[0].replace(/\/\.+\//, '');
	const pathname = Families.urls['/' + urlname];

	pathname
		? res.sendFile(path.join(__dirname, '..', config.PATH_FAMILIES, pathname))
		: res.status(404).send('Unknown file');
});

module.exports = router;
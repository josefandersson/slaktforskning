const path = require('path');
const pug = require('pug');

const FileUtil = require('./FileUtil');
const config = require('../config');

const IMAGE_EXTENSIONS = ['png', 'jpg', 'jpeg', 'webp', 'bmp', 'svg'];

const PATH_FAMILIES = path.resolve(__dirname, '..', config.PATH_FAMILIES);
const PATH_VIEWS = path.join(__dirname, '..', 'views');
const PATH_VIEW_FAMILIES = path.join(PATH_VIEWS, 'families.pug');
const PATH_VIEW_FAMILY = path.join(PATH_VIEWS, 'family.pug');

// Functions for generating page from files
// Cache generated pages, regenerate when files in files updates (listen for change)

const pages = { indexPage: '' };
const urls = {}; // url:path
let familyUrls = { urls: [] };
let familyFiles;

function updateFileTree() {
	familyFiles = FileUtil.dirToObject(PATH_FAMILIES).files;
	familyFiles.forEach(family => FileUtil.objectToPathTree(family, urls));
	familyUrls.urls = familyFiles.map(family => ({ name: family.name, url: family.url }));
}

const generateIndex = () => {
	const families = familyFiles.filter(file => file.files).map(family => {
		const imageFilename = family.files.filter(file => !file.files).find(file => IMAGE_EXTENSIONS.indexOf(path.extname(file.name)) > -1);
		return {
			image: imageFilename
				? path.join(family.name, imageFilename)
				: null,
			name: family.name,
			url: family.url
		};
	});
	pages.indexPage = pug.compileFile(PATH_VIEW_FAMILIES)({ currentPath: `/`, isValidSid: true, families });
};

function generateFamilyPages() {
	familyFiles.forEach(family => pages[family.url] = pug.compileFile(PATH_VIEW_FAMILY)({ currentPath: `/${family.url}`, isValidSid: true, family, families: familyUrls.urls }));
}

function generate() {
	updateFileTree();
	generateIndex();
	generateFamilyPages();
}

FileUtil.watchAndRun(config.PATH_FAMILIES, generate);

module.exports = {
	pages,
	urls,
	familyUrls
};
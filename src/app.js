const express = require('express');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const expressip = require('express-ip')
const path = require('path');

const config = require('../config');

const app = express();

app.set('views', path.join(__dirname, '..', 'views'))
app.set('view engine', 'pug')

app.use(logger('dev'));
app.use(expressip().getIpInfoMiddleware);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '..', 'public')));

app.use(require('../routes/AuthController'));
app.use(require('../routes/FamilyController'));

const port = process.env.PORT || config.PORT || 5500;
app.listen(port, () => console.log(`Running on http://localhost:${port}`));
const fs = require('fs');
const path = require('path');
const chokidar = require('chokidar');

function dirToObject(dirPath, depth = 0) {
	return {
		depth,
		files: fs.readdirSync(dirPath, { withFileTypes: true }).map(file => {
			let ordinalMatch = /^-?([0-9]+[,.]?\S*) /.exec(file.name);
			let ordinal = null, displayname = file.name;
			if (ordinalMatch) {
				ordinal = ordinalMatch[1].replace(/[-,]/g, '.');
				if (isFinite(+ordinal))
					ordinal = +ordinal
				displayname = displayname.replace(/^-?([0-9]+[,.]?\S*) /, '');
			}
			if (file.isDirectory())
				return { name: file.name, url: nameToUrl(file.name), ordinal, displayname, ...dirToObject(path.join(dirPath, file.name), depth + 1) }
			else
				return { name: file.name, url: nameToUrl(file.name), ordinal, displayname };
		}).sort((a, b) => (a.ordinal != null && b.ordinal != null
			? a.ordinal < b.ordinal
			: a.name < b.name) ? -1 : 1)
	};
}

function objectToPathTree(obj, currentTree = {}, currentUrl = '', currentPath = '') {
	if (obj.files)
		obj.files.forEach(file => objectToPathTree(file, currentTree, `${currentUrl}/${obj.url}`, `${currentPath}/${obj.name}`));
	else
		currentTree[`${currentUrl}/${obj.url}`] = `${currentPath}/${obj.name}`;
	return currentTree;
}

function watchAndRun(filePath, callback) {
	chokidar
		.watch(path.resolve(__dirname, '..', filePath), { ignoreInitial: true })
		.on('all', callback);
	callback('init', filePath);
}

const nameToUrl = name => name.trim().replace(/ +/g, '-').toLowerCase();

module.exports = {
	dirToObject,
	objectToPathTree,
	watchAndRun
}
module.exports = {
	PATH_FAMILIES: 'files/familjer',
	PATH_ACCESS_CODES: 'files/åtkomstkoder.txt',

	SESSION_LIFESPAN: 1000 * 60 * 60 * 24 * 30, // 1 month,
	IP_AUTH_ATTEMPTS_PER_HOUR: 15,

	PORT: 5500,
};